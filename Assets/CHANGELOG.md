# Changelog
All notable changes to this project will be documented in this file.

## [1.0.2] - 08-09-2019
### Changed
	-change dependency from utility v0.0.5 to exception v0.0.2

## [1.0.1] - 02-09-2019
### Added
	-add assembly definition

## [1.0.0] - 02-09-2019
### Added
	-Advanced Encryption Standard
	-HMACMD5
	-HMACSHA1, HMACSHA256, HMACSHA384, HMACSHA512
	-MKEncrypt (Machine key)
	-SHA1, SHA256, SHA384, SHA512
