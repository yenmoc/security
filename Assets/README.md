# Security

## What

* Contains AES, MD5

## Installation

```bash
"com.yenmoc.security":"https://gitlab.com/yenmoc/security"
or
npm publish --registry=http://localhost:4873
```

## Usages

### AES (CBC mode)

```csharp
		 AESEncryptor temp = new AESEncryptor("1234", EBits.Bits128);
        string data = "raw data contains in here 2019";
        Stopwatch watch = new Stopwatch();

        byte[] byteData = Encoding.UTF8.GetBytes(data);
        var encrypted = temp.Encrypt(byteData);

```

### MD5

```csharp
	string result = MD5.GetHashString(data);
	string result = MD5.GetHashString(data, Encoding.UTF8);
	byte[] result = MD5.GetHash(data);
	byte[] result = MD5.GetHash(data, Encoding.UTF8);


	var source = "Md5 hash data";      
    MD5.GetHashString(source);

```

### Base64Extensions

```csharp
	string Base64Encrypt(string input);
	string Base64Encrypt(string input, Encoding encoding);
	string Base64Decrypt(string input);
	string Base64Decrypt(string input, Encoding encoding);
```


### HMACSHA

```csharp
	string HMACSHA1(string source, string key);
	string HMACSHA256(string source, string key);
	string HMACSHA384(string source, string key);
	string HMACSHA512(string source, string key);
```

### MachineKey

```csharp
	MKEncrypt.ValidationKey(128);
	MKEncrypt.DecryptionKey(48);
```

### WIP [RSA, DES]