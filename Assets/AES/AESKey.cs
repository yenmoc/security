﻿// ReSharper disable once CheckNamespace
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace UnityModule.Security
{
    // ReSharper disable once InconsistentNaming
    public struct AESKey
    {
        /// <summary>
        /// ase key
        /// </summary>
        public byte[] Key { get; }

        /// <summary>
        /// ase IV
        /// </summary>
        public byte[] Iv { get; }

        public AESKey(byte[] key, byte[] iv)
        {
            Key = key;
            Iv = iv;
        }
    }
}