﻿// ReSharper disable once CheckNamespace

namespace UnityModule.Security
{
    // ReSharper disable once InconsistentNaming
    public enum EBits
    {
        Bits128 = 16,
        Bits192 = 24,
        Bits256 = 32
    }
}