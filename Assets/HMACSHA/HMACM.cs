﻿using System;
using System.Security.Cryptography;
using System.Text;
using UnityModule.Ex;

// ReSharper disable UnusedMember.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.Security
{
    // ReSharper disable once InconsistentNaming
    // ReSharper disable once UnusedMember.Global
    public class HMACM
    {
        #region HMACMD5

        /// <summary>
        /// HMACMD5 hash
        /// </summary>
        /// <param name="source">The string to be encrypted</param>
        /// <param name="key">encrypte key</param>
        /// <returns></returns>
        public static string D5(string source, string key)
        {
            if (string.IsNullOrEmpty(source))
            {
                throw Error.ArgumentNull("source");
            }

            if (string.IsNullOrEmpty(key))
            {
                throw Error.ArgumentNull("key");
            }

            using (var md5 = new HMACMD5(Encoding.UTF8.GetBytes(key)))
            {
                var strMd5Out = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(source)));
                strMd5Out = strMd5Out.Replace("-", "");
                return strMd5Out;
            }
        }

        #endregion
    }
}