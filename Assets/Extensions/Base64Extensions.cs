﻿using System;
using System.Text;
using UnityModule.Ex;

// ReSharper disable UnusedMember.Global
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable once CheckNamespace
namespace UnityModule.Security
{
    public static class Base64Extensions
    {
        /// <summary>
        /// Base64 encrypt default encoding = Encoding.UTF8
        /// </summary>
        /// <param name="input">input value</param>
        /// <returns></returns>
        public static string Base64Encrypt(string input)
        {
            return Base64Encrypt(input, Encoding.UTF8);
        }

        /// <summary>
        /// Base64 encrypt
        /// </summary>
        /// <param name="input">input value</param>
        /// <param name="encoding">text encoding</param>
        /// <returns></returns>
        public static string Base64Encrypt(string input, Encoding encoding)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw Error.ArgumentNull("input");
            }

            return Convert.ToBase64String(encoding.GetBytes(input));
        }

        /// <summary>
        /// Base64 decrypt default encoding = Encoding.UTF8
        /// </summary>
        /// <param name="input">input value</param>
        /// <returns></returns>
        public static string Base64Decrypt(string input)
        {
            return Base64Decrypt(input, Encoding.UTF8);
        }

        /// <summary>
        /// Base64 decrypt
        /// </summary>
        /// <param name="input">input value</param>
        /// <param name="encoding">text encoding</param>
        /// <returns></returns>
        public static string Base64Decrypt(string input, Encoding encoding)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw Error.ArgumentNull("input");
            }

            return encoding.GetString(Convert.FromBase64String(input));
        }
    }
}